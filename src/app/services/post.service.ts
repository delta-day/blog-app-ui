import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../models/Post';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {
 //service class
  //what is the url in the Backend for our GetMapping method?
  //http://localhost:8080/api/v1/posts

  private  getUrl: string = "http://localhost:8080/api/v1/posts";//links backend with frontend

  //using dependency injection, bring it the HttpClient
  constructor(private httpClient: HttpClient) { }// helps us make requests


  //method that will make a request to the GetMapping method in our backend
  getPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(this.getUrl).pipe(
      map(result => result)
    )
  }
  //method that will make a request to the PostMapping method in our backend
  savePost(newPost: Post): Observable<Post> {
    return this.httpClient.post<Post>(this.getUrl, newPost);
  }

  //Feature 3 frontend after finishing backend
  //method that will make a request to the GetMapping method
  // to view an individual Post from our backend
  viewPost(id: number): Observable<Post> {
    return this.httpClient.get<Post>(`${this.getUrl}/${id}`).pipe(
      map(result => result)
    )
  }

  //Feature 4
  //method that will make a request to the DeleteMapping method
  deletePost(id: number): Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'})
  }


}
