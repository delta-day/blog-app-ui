import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from 'src/app/models/Post';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  //properties
  newPost: Post = {
    id: 0,
    title: '',
    description: ''
  };//empty object


  //make request from component to our service = dependency injection
  //create variable
  constructor(private postSvc: PostService,
              private router: Router,
              private activeRoute: ActivatedRoute) { }// router redirects to home page

  ngOnInit(): void {

    //feature 3
    var isIDPresent = this.activeRoute.snapshot.paramMap.has("id");

    if (isIDPresent) {
      const id = this.activeRoute.snapshot.paramMap.get('id');
      this.postSvc.viewPost(Number(id)).subscribe(
        data => this.newPost = data
      )
    }


  }

  //method that will make a request to the savePost() method in our service
  // and will redirect the user to /posts aka our home page
  savedPost() {
    this.postSvc.savePost(this.newPost).subscribe(
      data => {
        this.router.navigateByUrl("/posts");
      }
    )
  }
  //test method created to delete
  deletedPost(id: number) {
    this.postSvc.deletePost(id).subscribe(
      data => this.router.navigateByUrl("/posts")
    )
  }







}
